from django.shortcuts import render
from django.http import HttpResponse
from .models import Status
from .forms import Form_Status
# Create your views here.

response={}
def index(request):
    mystatus = Status.objects.all()
    response['mystatus'] = mystatus
    response['form'] = Form_Status
    html = 'Hello.html'
    form = Form_Status(request.POST or None)
    if(request.method =='POST'):
        response['mystatus'] = request.POST['mystatus']
        save_status = Status(mystatus = response['mystatus']) 
        save_status.save()
        save_status = Status.objects.all()
        response['mystatus'] = save_status
    return render(request, html, response)


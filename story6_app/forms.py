from django.db import models
from django import forms
from .models import Status

class Form_Status(forms.Form):
    attrs = {
        'type' : 'text',
        'class' : 'todo-form-input',
        'placeholder' : 'What is on your mind?' 
    }
    mystatus = forms.CharField(label = '',required = True, max_length = 300,widget = forms.TextInput(attrs=attrs))

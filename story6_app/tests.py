from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Status
from selenium import webdriver
import unittest
import time
# Create your tests here.
class story6UnitTest(TestCase):

    def test_lab_6_url_is_exist(self):
        response = Client().get('/story6_app/')
        self.assertEqual(response.status_code, 200)

    def test_hello_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_template_hello(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'Hello.html')

    def test_landing_page_(self):
        response = Client().get('/')
        html_response = response.content.decode('utf-8')
        self.assertIn('Hallo, Apa kabar?',html_response)
    def test_create_object_model (self):
        status_message = Status.objects.create(mystatus = 'ok')
        counting_object_status = Status.objects.all().count()
        self.assertEqual(counting_object_status, 1)

class Story6FuncTest(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Chrome();

    def tearDown(self):
        self.browser.implicitly_wait(3)
        self.browser.quit()

    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get('http://127.0.0.1:8000/')
        self.assertIn('Hello', self.browser.title) #
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Hallo, Apa kabar?', header_text)

    def test_input_todo(self):
        driver = webdriver.Chrome()
        # Opening the link we want to test
        driver.get('http://127.0.0.1:8000/')
        
        # find the form element
        statusku = driver.find_element_by_name('mystatus')
       
        submit = driver.find_element_by_name('submit')
        # Fill the form with data
        statusku.send_keys('Coba Coba')
        
        # submitting the form
        submit.submit()
        time.sleep(5)
        driver.quit()
            

if __name__ == '__main__': #
    unittest.main(warnings='ignore') #

